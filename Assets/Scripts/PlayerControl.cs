﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    public int PlayerNumber = 1;
    Transform enemy;

    Rigidbody2D rb2d;
    Animator anim;

    float horizontal;
    float vertical;
    public float maxSpeed = 25;
    Vector3 movement;
    bool crouch;

    public float jumpForce = 20;
    public float jumpDuration = .1F;
    private float jmpForce;
    private float jmpDuration;
    bool jumpKey;
    bool falling;
    bool onGround;

    public float attackRate = 0.3f;
    bool[] attack = new bool[2];
    float[] attacktimmer = new float[2];
    int[] timePressed = new int[2];

    public bool damage;
    public float noDamage = 1;
    float noDamageTimer;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();

        jmpForce = jumpForce;

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject pl in players)
        {
            if (pl.transform != this.transform)
                enemy = pl.transform;
        }

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        horizontal = Input.GetAxis("Horizontal" + PlayerNumber.ToString());
        vertical = Input.GetAxis("Vertical" + PlayerNumber.ToString());

        movement = new Vector3(horizontal, 0, 0);

        crouch = (vertical < -0.1f);

        if (vertical > 0.1f)
        {
            if (!jumpKey)
            {
                jmpDuration += Time.deltaTime;
                jmpForce += Time.deltaTime;

                if (jmpDuration < jumpDuration)
                {
                    rb2d.velocity = new Vector2(rb2d.velocity.x, jmpForce);
                }
                else
                {
                    jumpKey = true;
                }
            }
        }

        if (!onGround && vertical < .1F)
        {
            falling = true;
        }

        if (attack[0] && !jumpKey || attack[1] && !jumpKey)
        {
            movement = Vector2.zero;
        }

        if (!crouch)
            rb2d.AddForce(movement * maxSpeed);
        else
            rb2d.velocity = Vector3.zero;
	}

    void Update()
    {
        AttackInput();
        ScaleCheck();
        Damage();
        OnGroundCheck();
        UpdateAnimator();
    }

    void ScaleCheck()
    {
        if (transform.position.x < enemy.transform.position.x)
            transform.localScale = new Vector3(-1, 1, 1);
        else
            transform.localScale = Vector3.one;
    }

    void Damage()
    {
        if (damage)
        {
            noDamageTimer += Time.deltaTime;

            if (noDamageTimer > noDamage)
            {
                damage = false;
                noDamageTimer = 0;
            }
        }

        /*if (!onGround)
        {
            rb2d.gravityScale = 10;
            Vector3 dir = enemy.position - transform.position;
            rb2d.AddForce(-dir * 25);
        }*/
    }

    void AttackInput()
    {
        if (Input.GetButtonDown("Attack1" + PlayerNumber.ToString()))
        {
            attack[0] = true;
            attacktimmer[0] = 0;
            timePressed[0]++;
        }

        if(attack[0])
        {
            attacktimmer[0] += Time.deltaTime;

            if (attacktimmer[0] > attackRate || timePressed[0] >= 4)
            {
                attacktimmer[0] = 0;
                attack[0] = false;
                timePressed[0] = 0;
            }
        }

        if (Input.GetButtonDown("Attack2" + PlayerNumber.ToString()))
        {
            attack[1] = true;
            attacktimmer[1] = 0;
            timePressed[1]++;
        }

        if (attack[1])
        {
            attacktimmer[1] += Time.deltaTime;

            if (attacktimmer[1] > attackRate || timePressed[1] >= 4)
            {
                attacktimmer[1] = 0;
                attack[1] = false;
                timePressed[1] = 0;
            }
        }
    }

    void OnGroundCheck()
    {
        if (!onGround)
            rb2d.gravityScale = 5;
        else
            rb2d.gravityScale = 1;
    }

    void UpdateAnimator()
    {
        anim.SetBool("Crouch", crouch);
        anim.SetBool("OnGround", this.onGround);
        anim.SetBool("Falling", falling);
        anim.SetFloat("Movement", Mathf.Abs(horizontal));
        anim.SetBool("Attack1", attack[0]);
        anim.SetBool("Attack2", attack[1]);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.tag == "Ground")
        {
            onGround = true;

            jumpKey = false;
            jmpDuration = 0;
            jmpForce = jumpForce;
            falling = false;
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.collider.tag == "Ground")
        {
            onGround = false;
        }
    }
}
