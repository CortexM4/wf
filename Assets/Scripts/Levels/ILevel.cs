﻿//using UnityEngine;
//using System.Collections;

public interface ILevel  {

    int current_experience { get; set; }
    int nextLevelExp { get; }

}
