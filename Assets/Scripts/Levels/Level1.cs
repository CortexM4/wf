﻿using UnityEngine;

public class Level1 : ILevel
{

    public int current_experience { get; set; }

    public int nextLevelExp
    {
        get { return 1000; }
    }
}
