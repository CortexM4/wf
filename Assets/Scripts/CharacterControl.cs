﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterControl : MonoBehaviour {

	public float m_health = 100f;
    public int m_PlayerNumber = 1;              // Used to identify which character belongs to which player.  This is set by this character's manager.
	public float m_Speed = 12f;                 // How fast the character moves right and left.
	public float m_JumpForce = 800;

    /* Элементы управления: оси, кнопки и т.д. */
    private string m_HorizontalAxisName;
    private string m_VerticalAxisName;
    private string m_AttackOneBtnName;
    private string m_AttackTwoBtnName;
    private string m_BlockBtnName;
    
    /* Внутренние состояния */
    private float m_HorizontalInputValue;        // The current value of the Horizontal input.
    private float m_VerticalInputValue;          // The current value of the Vertical input.
    private bool m_AttackOne;
    private bool m_AttackTwo;
    private bool m_Blocking = false;
    private bool m_grounded = false;
    
    private StateMachine stateMachine;
    public StateMachine _StateMachine { get { return stateMachine; } }

    private ILevel m_level;

    private Rigidbody2D m_Rigidbody;
    private Animator m_anima;
    private Slider m_Slider;

	private GameObject m_enemy;
    private Transform m_enemyTransform;
    private CharacterControl m_enemyCharacterControl;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_anima = GetComponentInChildren<Animator>();
        m_Slider = GetComponentInChildren<Slider>();
        stateMachine = new StateMachine(m_Rigidbody, m_anima);
    }

    private void OnEnable()
    {
        // When the tank is turned on, make sure it's not kinematic.
        m_Rigidbody.isKinematic = false;

        // Also reset the input values.
        //m_MovementInputValue = 0f;
        //m_TurnInputValue = 0f;
    }


    private void OnDisable()
    {
        // When the tank is turned off, set it to kinematic so it stops moving.
        m_Rigidbody.isKinematic = true;
    }

	// Use this for initialization
	private void Start () 
    {
        // Find enemy player
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (player.transform != this.transform)
            {
				m_enemy = player;
                m_enemyTransform = m_enemy.GetComponent<Transform>();
                m_enemyCharacterControl = m_enemy.GetComponent<CharacterControl>();
            }
        }
        // The axes names are based on player number.
        m_VerticalAxisName = "Vertical_" + m_PlayerNumber;
        m_HorizontalAxisName = "Horizontal_" + m_PlayerNumber;
        m_AttackOneBtnName = "AttackOne_" + m_PlayerNumber;
        m_AttackTwoBtnName = "AttackTwo_" + m_PlayerNumber;
        m_BlockBtnName = "Block_" + m_PlayerNumber;
	}
	
	// Update is called once per frame
	private void Update () {
        // Store the value of both input axes.
        m_HorizontalInputValue = Input.GetAxisRaw(m_HorizontalAxisName);
		m_VerticalInputValue = Input.GetAxisRaw(m_VerticalAxisName);
        m_AttackOne = Input.GetButtonDown(m_AttackOneBtnName);
        m_AttackTwo = Input.GetButtonDown(m_AttackTwoBtnName);
        m_Blocking = Input.GetButton(m_BlockBtnName);

        CheckPosition();
        UpdateAnimation();
        UpdateGUI();
        

        //EngineAudio();
	}

    private void FixedUpdate()
    {
        m_grounded = (m_Rigidbody.velocity.y == 0); // Check ground
        stateMachine.isGrounded = m_grounded;       // Это вообще липа. Может перенести StateMachine в класс CharacterControl?

        // Idle State
		if( m_grounded && m_VerticalInputValue == 0 && m_HorizontalInputValue == 0 && !m_Blocking && !m_AttackOne)  // Надо подумать на счет m_Blocking и т.д.
			stateMachine.stopPlayer();
		// Move State
        if (m_HorizontalInputValue != 0)
            stateMachine.movePlayer(m_Speed, (int)m_HorizontalInputValue); 
        
		// Jump State
		if( m_VerticalInputValue == 1.0F )
			stateMachine.jumpPlayer(m_JumpForce);

		if(m_AttackOne)
			stateMachine.attackOne();

        if (m_AttackTwo)
            stateMachine.attackTwo();

        if (m_Blocking)
            stateMachine.blockPlayer();

    }

    private void CheckPosition()
    {
        if (m_enemy == null)
            return;
        if (transform.position.x > m_enemyTransform.position.x)
            transform.localScale = transform.localScale = new Vector3(-1, 1, 1);
        else
            transform.localScale = Vector3.one;
    }

    private void UpdateAnimation()
    {
        //m_anima.SetFloat("IsWalking", Mathf.Abs(m_Rigidbody.velocity.x));
        m_anima.SetBool("IsMoving", stateMachine.IsMoving);
        m_anima.SetBool("IsGrounded", m_grounded);
        m_anima.SetBool("IsAttackOne", stateMachine.IsAttackOne);
        m_anima.SetBool("IsAttackTwo", stateMachine.IsAttackTwo);
        m_anima.SetBool("IsBlocking", m_Blocking);
        m_anima.SetBool("IsJerk", stateMachine.IsJerk);
    }

    private void UpdateGUI()
    {
        m_Slider.value = m_health;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "AttackOne" && m_Blocking)     // Возможно тут надо только условие на блок без типа атаки
        {
            Debug.Log("BLOCK!!!!!!!!!!!!!");
            m_anima.SetTrigger("AttackDefence");
            m_Rigidbody.AddForce(new Vector2(1000 * -transform.localScale.x , 0));
        }
        if (other.tag == "AttackOne" && !m_Blocking)
        {
            Debug.Log("---------------> Attack One in head");
            m_anima.SetTrigger("AttackOneDamage");
            m_Rigidbody.AddForce(new Vector2(2000 * -transform.localScale.x, 0));
            m_health -= m_enemyCharacterControl._StateMachine.Damage;
        }
        if (other.tag == "AttackTwo" && !m_Blocking)
        {
            Debug.Log("---------------> Attack two in head");
            m_anima.SetTrigger("AttackTwoDamage");
            m_Rigidbody.AddForce(new Vector2(200 * -transform.localScale.x, 400));
            m_health -= m_enemyCharacterControl._StateMachine.Damage;
        }
    }
}
