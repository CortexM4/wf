﻿using UnityEngine;

public class BlockState : IState {
	
	public float m_duration { get; set; }
	// Max duration of action for JumpState
	public float actionTime 
	{ 
		get
		{
			return .07F;			
		}
	}
	
	StateMachine stateMachine;
	
	public BlockState(StateMachine stateMachine)
	{
		this.stateMachine = stateMachine;
	}
	
	public void stopPlayer()
	{
        //Debug.Log("Stop from Blocking state");
        stateMachine.IsMoving = false;
        stateMachine.SetState(stateMachine.GetIdleState(), this);
	}
	
	public void movePlayer(float moveSpeed, int direction)
	{
        // Движемся в блоке в два раза медленнее,
        // Странно, что move в MoveState в которое переключаемся не срабатывает,
        // т.е. при отключеннии движения тут, персонаж вообще стоит!

        stateMachine.IsMoving = true;
        stateMachine.CRigidbody.velocity = new Vector2(moveSpeed * direction / 2, stateMachine.CRigidbody.velocity.y); 
        //stateMachine.CRigidbody.velocity = Vector2.zero;
        stateMachine.SetState(stateMachine.GetMoveState(), this);
	}
	
	public void jumpPlayer(float jumpForce)
	{
		throw new System.NotImplementedException();
	}
	
	
	public void jerkPlayer(float force)
	{
		throw new System.NotImplementedException();
	}

	public void blockPlayer()
	{
        stateMachine.IsMoving = false;
        stateMachine.CRigidbody.velocity = Vector2.zero;
	}
	
	public void attackOne()
	{
        stateMachine.IsAttackOne = true;
        //stateMachine.GetAttackOneState().m_duration = 0;
        stateMachine.SetState(stateMachine.GetAttackOneState(), this);
	}
	
	
	public void specialAbility()
	{
		throw new System.NotImplementedException();
	}


    public void attackTwo()
    {
        stateMachine.IsAttackTwo = true;
        //stateMachine.GetAttackTwoState().m_duration = 0;
        stateMachine.SetState(stateMachine.GetAttackTwoState(), this);
    }
}
