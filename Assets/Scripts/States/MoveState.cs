﻿using UnityEngine;

public class MoveState : IState {

	public float m_duration { get; set; }

	// Max duration of action for MoveState
	public float actionTime 
	{ 
		get
		{
			return .1F;			
		}
	}

	public int count;

    StateMachine stateMachine;

    public MoveState(StateMachine stateMachine)
    {				
		this.stateMachine = stateMachine;
		m_duration = 0;
		count = 0;
    }

    public void stopPlayer()
    {
        stateMachine.IsMoving = false;
        stateMachine.CRigidbody.velocity = Vector2.zero;
		stateMachine.SetState(stateMachine.GetIdleState(), this);
        /* Анимация Idle */
        /* Эта анимация тут не нужна, оставлю в коменте на всякий случай */
        //stateMachine.AnimatorChar.SetBool("IsGrounded", true);
    }

    public void movePlayer(float moveSpeed, int direction)
    {
		m_duration += Time.deltaTime;

        /* Физика движения */
        stateMachine.CRigidbody.velocity = new Vector2(moveSpeed * direction, stateMachine.CRigidbody.velocity.y);

    }

    public void jumpPlayer(float jumpForce)
    {
        //stateMachine.IsMoving = false;
        stateMachine.CRigidbody.AddForce(new Vector2(0, jumpForce));
		stateMachine.SetState(stateMachine.GetJumpState(), this);
    }


    public void jerkPlayer(float force)
    {
        throw new System.NotImplementedException();
    }

	public void blockPlayer()
	{
		//throw new System.NotImplementedException();
        stateMachine.SetState(stateMachine.GetBlockState(), this);
	}


    public void attackOne()
    {
        stateMachine.CRigidbody.velocity = Vector2.zero;
        stateMachine.IsAttackOne = true;
        stateMachine.SetState(stateMachine.GetAttackOneState(), this);
    }


    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    public void attackTwo()
    {
        throw new System.NotImplementedException();
    }
}
