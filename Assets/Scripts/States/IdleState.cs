﻿using UnityEngine;
using System.Collections;

public class IdleState : IState {

	public float m_duration { get; set; }
	// Max duration of action for IdleState
	public float actionTime 
	{ 
		get
		{
			return .1F;			
		}
	}

    StateMachine stateMachine;

    public IdleState(StateMachine stateMachine)
    {
        m_duration = 0;
		this.stateMachine = stateMachine;
    }

    public void stopPlayer()
    {
        m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
        //m_Rigidbody.velocity = Vector2.zero;
    }

    public void movePlayer(float moveSpeed, int direction)
    {
        IState m_lastState = stateMachine.GetLastState();
        if (m_lastState is MoveState)
        {
            if (m_lastState.m_duration < m_lastState.actionTime && this.m_duration < this.actionTime)
            {
                /* На самом деле надо будет подумать, может сделать рекурсию */
                stateMachine.IsMoving = false;
                stateMachine.IsJerk = true;
                //stateMachine.AnimatorChar
                m_duration = 0;
                m_lastState.m_duration = 0;

                stateMachine.SetState(stateMachine.GetJerkState(), this);
                stateMachine.jerkPlayer(30 * direction);                  // Надо эти 50 превратить в что-то более человеческое
                return;
            }
        }

        stateMachine.IsMoving = true;

        m_duration = 0;
        m_lastState.m_duration = 0;

        /* Физика движения */
        //m_Rigidbody.velocity = new Vector2(moveSpeed * direction, m_Rigidbody.velocity.y);
        stateMachine.CRigidbody.velocity = new Vector2(moveSpeed * direction, stateMachine.CRigidbody.velocity.y);

		stateMachine.SetState(stateMachine.GetMoveState(), this);
    }

    public void jumpPlayer(float jumpForce)
    {
        m_duration = 0;
        stateMachine.CRigidbody.AddForce(new Vector2(0, jumpForce));
		//rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
		stateMachine.SetState(stateMachine.GetJumpState(), this);
    }


    public void jerkPlayer(float force)
    {
        throw new System.NotImplementedException();
    }

	public void blockPlayer()
	{
        stateMachine.SetState(stateMachine.GetBlockState(), this);
	}

    /***********  Разные атаки первого уровня из состояния Idle  ***********/
    public void attackOne()
    {
        stateMachine.IsAttackOne = true;
        //stateMachine.GetAttackOneState().m_duration = 0;
        stateMachine.SetState(stateMachine.GetAttackOneState(), this);
    }

    public void attackTwo()
    {
        stateMachine.IsAttackTwo = true;
        //stateMachine.GetAttackTwoState().m_duration = 0;
        stateMachine.SetState(stateMachine.GetAttackTwoState(), this);
    }


    /***********  Специальные типа атак и возможностей из состояния Idle  ***********/
    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    
}
