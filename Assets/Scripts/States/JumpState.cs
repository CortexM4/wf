﻿using UnityEngine;

public class JumpState : IState {

	public float m_duration { get; set; }
	// Max duration of action for JumpState
	public float actionTime 
	{ 
		get
		{
			return .07F;			
		}
	}

	StateMachine stateMachine;

    public JumpState(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
    }

    public void stopPlayer()
    {
        stateMachine.IsMoving = false;
        stateMachine.CRigidbody.velocity = Vector2.zero;
		stateMachine.SetState(stateMachine.GetIdleState(), this);
    }

    public void movePlayer(float moveSpeed, int direction)
    {
		/*
		 * 	Это конечно трешачек
         * 	Определяем, что если мы на земле, то меняем состояние
		 */
        if (stateMachine.isGrounded)
        {
            stateMachine.IsMoving = true;
            stateMachine.CRigidbody.velocity = new Vector2(moveSpeed * direction, stateMachine.CRigidbody.velocity.y);
            stateMachine.SetState(stateMachine.GetMoveState(), this);
        }
    }

    public void jumpPlayer(float jumpForce)
    {
        // Not implemented
    }


    public void jerkPlayer(float force)
    {
        throw new System.NotImplementedException();
    }

	public void blockPlayer()
	{
		throw new System.NotImplementedException();
	}


    public void attackOne()
    {
        throw new System.NotImplementedException();
    }


    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    public void attackTwo()
    {
        throw new System.NotImplementedException();
    }
}
