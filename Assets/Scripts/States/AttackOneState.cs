﻿using System;
using UnityEngine;

public class AttackOneState : IState, IDamage {

	public float m_duration { get; set; }
	// Max duration of action for JumpState
	public float actionTime 
	{ 
		get
		{
			return .35f;			
		}
	}

    public float damage { get { return 10f; } }

    StateMachine stateMachine;

    public AttackOneState(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
    }

    public void stopPlayer()
    {
        //Debug.Log(m_duration);
        m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
        if (m_duration > actionTime)
        {
            stateMachine.IsAttackOne = false;
            stateMachine.SetState(stateMachine.GetIdleState(), this);
        }
    }

    public void movePlayer(float moveSpeed, int direction)
    {
        m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
         if (m_duration > actionTime)
         {
             stateMachine.IsAttackOne = false;
             stateMachine.SetState(stateMachine.GetMoveState(), this);
         }
    }

    public void jumpPlayer(float jumpForce)
    {
        throw new System.NotImplementedException();
    }


    public void jerkPlayer(float force)
    {
        throw new System.NotImplementedException();
    }

	public void blockPlayer()
	{
        m_duration += Time.deltaTime;
        if (m_duration > actionTime)
        {
            stateMachine.IsAttackOne = false;
            stateMachine.SetState(stateMachine.GetBlockState(), this);
        }
        //if (!stateMachine.IsAttackOne)
        //    stateMachine.SetState(stateMachine.GetBlockState(), this);
    }

    public void attackOne()
    {
        m_duration += Time.deltaTime;
        if (m_duration > actionTime)
        {
            stateMachine.IsAttackOne = true;
            m_duration = 0;
            stateMachine.SetState(stateMachine.GetAttackOneState(), this);
        }
    }


    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    public void attackTwo()
    {
        throw new System.NotImplementedException();
    }
}
