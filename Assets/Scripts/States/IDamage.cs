﻿
public interface IDamage {
    float damage { get; }
}
