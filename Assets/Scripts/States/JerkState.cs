﻿using UnityEngine;

public class JerkState : IState {

	public float m_duration { get; set; }
    public float actionTime
    {
        get { return .5F; }
    }

	StateMachine stateMachine;

    public JerkState(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
    }

    public void stopPlayer()
    {
        // А вот сюда мы вообще попадем?
        // Но в любом случае тормазнем
        m_duration += Time.deltaTime;
        if (m_duration > actionTime)
        {
            stateMachine.IsJerk = false;
            stateMachine.CRigidbody.velocity = Vector2.zero;
            stateMachine.SetState(stateMachine.GetIdleState(), this);
        }
    }

    public void movePlayer(float moveSpeed, int direction)
    {
        m_duration += Time.deltaTime;
        if (m_duration > actionTime)
        {
            stateMachine.IsJerk = false;
            stateMachine.CRigidbody.velocity = Vector2.zero;
            stateMachine.SetState(stateMachine.GetIdleState(), this);
        }
    }

    public void jerkPlayer()
    {
        throw new System.NotImplementedException();
    }

    public void jumpPlayer(float jumpForce)
    {
        throw new System.NotImplementedException();
    }

    public void jerkPlayer(float force)
    {
        stateMachine.IsJerk = true;
        //m_duration = 0;
        //stateMachine.CRigidbody.velocity = new Vector2(force, stateMachine.CRigidbody.velocity.y);
        stateMachine.CRigidbody.AddForce(new Vector2(force, 0), ForceMode2D.Impulse);
    }

	public void blockPlayer()
	{
		throw new System.NotImplementedException();
	}


    public void attackOne()
    {
        throw new System.NotImplementedException();
    }


    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    public void attackTwo()
    {
        throw new System.NotImplementedException();
    }
}
