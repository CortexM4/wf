﻿using UnityEngine;

public class AttackTwoState : IState, IDamage
{

    public float m_duration { get; set; }
    // Max duration of action for JumpState
    public float actionTime
    {
        get
        {
            return .35F;
        }
    }

    public float damage { get { return 10f; } }

    StateMachine stateMachine;

    public AttackTwoState(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
    }

    public void stopPlayer()
    {
        m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
        if (m_duration > actionTime)
        {
            stateMachine.IsAttackTwo = false;
            stateMachine.SetState(stateMachine.GetIdleState(), this);
        }
    }

    public void movePlayer(float moveSpeed, int direction)
    {
        m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
        if (m_duration > actionTime)
        {
            stateMachine.IsAttackTwo = false;
            stateMachine.SetState(stateMachine.GetMoveState(), this);
        }
    }

    public void jumpPlayer(float jumpForce)
    {
        throw new System.NotImplementedException();
    }


    public void jerkPlayer(float force)
    {
        throw new System.NotImplementedException();
    }

    public void blockPlayer()
    {
        m_duration += Time.deltaTime;
        if (m_duration > actionTime)
        {
            stateMachine.IsAttackTwo = false;
            stateMachine.SetState(stateMachine.GetBlockState(), this);
        }
    }

    public void attackOne()
    {
        throw new System.NotImplementedException();
    }


    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    public void attackTwo()
    {
        throw new System.NotImplementedException();
    }
}
